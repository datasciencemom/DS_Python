import numpy as np
import pandas as pd

def ts_to_ml(df, col_shift='t', n_in_max=1, n_in_min=0, n_out=1, dropnan=True):
    """
    Frame a time series as a supervised learning dataset.
    Arguments:
        data: time series data frame
        col_shift: time serie column to be shifted
        n_in_max: Number of maximum lag observations as input (X).
        n_in_min: Number of minimum lag observations as input (X).
        n_out: Number of observations as output (y).
        dropnan: Boolean whether or not to drop rows with NaN values.
    Returns:
        Pandas DataFrame of series framed for supervised learning.
    """
    df = df.rename(columns={col_shift:'t'})
    # input sequence (t-n, ... t-1)
    for i in range(n_in_max, n_in_min, -1):
        df['t' + '_minus_' + str(i)] = df['t'].shift(i)   
    # forecast sequence (t, t+1, ... t+n)
    for i in range(1, n_out):
        df['t' + '_plus_' + str(i)] = df['t'].shift(-i)   
    # drop rows with NaN values
    if dropnan:
        df = df.dropna(how='any')
    # change column name 
    df.columns = [w.replace('_minus_', '-') for w in list(df)]
    df.columns = [w.replace('_plus_', '-') for w in list(df)]
    return df


# split a univariate dataset into train/test sets
def train_test_split(df, pct_test):
    n_test = int(df.shape[0]*pct_test)
    return df[:-n_test], df[-n_test:]

