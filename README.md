Contents
- [x] data cleaning: duplicates, missing values, groupby 
- [x] data manipulation: long table and wide table transformation
- [x] machine learning models
- [x] neural network
